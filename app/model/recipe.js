const mongoose = require('mongoose');               

const RecipeSchema = mongoose.Schema ({
dish: String,
ingredient: String, 
steps: String,
recipe_type: String,
image: String
}
)
module.exports=mongoose.model('recipe', RecipeSchema);

