const users = require('../../app/model/users');
const tokenconfig = require('../../app/config/token');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

module.exports = {

    createUser: function (req, res, next) {

        users.findOne({
            email: req.body.emailaddress
        }, function (err, userInfo) {

            if (err) {
                next(err);
            } else {
                if (userInfo) {
                    return res.send({
                        status: false,
                        message: 'Email Address Already exists'
                    });
                } else {
                    bcrypt.hash(req.body.password, 10, function (err, hash) {
                        console.log(hash);
                        if (err) {
                            next(err);
                        } else {
                            users.create({
                                username: req.body.username,
                                password: hash,
                                email: req.body.emailaddress
                            }, function (err, userInfo) {
                                if (err)
                                    next(err);
                                else
                                    res.json({
                                        status: true,
                                        message: "Successfully Registered",
                                        userInfo
                                    });
                            })
                        }
                    })

                }
            }
        })
    },

    LoginUser: function (req, res, next) {

        users.findOne({
            email: req.body.emailaddress,

        }, function (err, userInfo) {

            if (err) {
                next(err);
            } else {
                if (userInfo == null) {
                    return res.status(200).send({
                        status: false,
                        message: 'Incorrect Email Address'
                    });
                } else {
                    bcrypt.compare(req.body.password, userInfo.password, function (err, result) {
                        if (result == true) {
                            const token = jwt.sign({
                                    email: userInfo.emailaddress
                                },
                                tokenconfig.secret, {
                                    expiresIn: "30 days" // expires in 24 hours
                                }
                            );

                            return res.status(200).send({
                                status: true,
                                message: 'You have successfully logged into the system',
                                token: token,
                                userInfo
                            });
                        } else {
                            return res.status(200).send({
                                status: false,
                                message: 'Incorrect Password'
                            });
                        }
                    })

                }
            }
        })
    },


}