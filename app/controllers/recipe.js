const recipe = require('../../app/model/recipe');
const tokenconfig = require('../../app/config/token');
const form_data_s3 = require('../../app/helper/form-data-s3');

module.exports = {

    viewOneRecipe: function (req, res, next) {
        recipe.findOne({
            _id: req.params.recipeId
        }, function (err, recipeInfo) {
            if (err) {
                next(err);
            } else {

                return res.status(200).send({
                    status: true,
                    message: 'Recipe Info has been Retrieved',
                    recipeInfo
                });

            }


        })
    },

    createRecipe: function (req, res, next) {
        
        form_data_s3.upload(req, res, "muthubhai", function (req, res) {
            if (req.file == undefined) {
                console.log('Error: No File Selected!');
                res.json('Error: No File Selected');
            } else {

                recipe.create({
                    dish: req.body.dish,
                    ingredient: req.body.ingredients,
                    steps: req.body.steps,
                    recipe_type: req.body.recipe_type,
                    image: req.file.location

                }, function (err, result) {
                    if (err)
                        next(err);
                    else

                        res.json({
                            status: true,
                            message: "Recipe has been successfully added",
                            product: result,

                        });
                })
            }
        })
    
    },

        deleteRecipe: function (req, res, next) {

            recipe.findByIdAndRemove(req.params.recipeId)
            .then(recipeDetails => {
                if (!recipeDetails) {
                    return res.send({
                        message: "Recipe not found with id " + req.params.recipeId,
                        status: false
                    });
                } else {
                    res.send({
                        message: "Recipe deleted successfully!",
                        status: true
                    });
                }
            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.send({
                        message: "Recipe not found with id " + req.params.recipeId,
                        status: false
                    });
                }
                return res.send({
                    message: "Could not delete the recipe with id " + req.params.recipeId,
                    status: false
                });
            });

        },

        viewRecipe: function (req, res, next) {
            recipe.find({
                recipe_type: req.params.recipeType
            }, function (err, allRecipeInfo) {
                if (err) {
                    next(err);
                } else {
                    return res.status(200).send({
                        status: true,
                        message: 'Recipe Info has been Retrieved',
                        allRecipeInfo
                    });
    
                }
            })
        }

}