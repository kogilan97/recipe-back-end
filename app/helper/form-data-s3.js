const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require('multer');
const path = require('path');

const s3 = new aws.S3({
    accessKeyId: "AKIAVTBIYANFVZMBHJ2F",
    secretAccessKey: "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",
});

module.exports.upload = function (req, res, bucket, callback) {

    const upload = multer({
        storage: multerS3({
            s3: s3,
            bucket: bucket,
            acl: 'public-read',
            key: function (req, file, cb) {
                cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
            }
        }),
        limits: {
            fileSize: 2000000
        },
        fileFilter: function (req, file, cb) {
            checkFileType(file, cb);
        }
    }).single('image');

    function checkFileType(file, cb) {
        // Allowed ext
        const filetypes = /jpeg|jpg|png|gif/;
        // Check ext
        const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        // Check mime
        const mimetype = filetypes.test(file.mimetype);
        if (mimetype && extname) {
            return cb(null, true);
        } else {
            cb('Error: Images Only!');
        }
    }


    upload(req, res, (err) => {

        if (err) {
            console.log(err)
        } else {
            callback(req, res)
        }

    });

}

exports.deleteS3 = function (bucket, imageName) {
    s3.deleteObject({
        Bucket: bucket,
        Key: imageName
    }, function (err, data) {
        if (err) {
            console.log(err)
        }
    })
}