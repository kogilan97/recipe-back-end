const express = require('express');
const recipe = require('../controllers/recipe');

const router = express.Router();

router.get('/findOne/:recipeId', recipe.viewOneRecipe);
router.get('/find/:recipeType', recipe.viewRecipe);
router.post('/create', recipe.createRecipe);
router.delete('/delete/:recipeId', recipe.deleteRecipe);


module.exports = router;