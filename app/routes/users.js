const express = require('express');
const users = require('../controllers/users');
const verifytoken = require('../helper/jwtauth');

const router = express.Router();

router.post('/register', users.createUser);

//Login a user
router.post('/login', users.LoginUser);

module.exports = router;