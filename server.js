const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const cors = require('cors');
const mongoose1 = require('./app/config/database');
const userRouter = require('./app/routes/users');
const recipeRouter = require('./app/routes/recipe');

const app = express();

app.use(logger('dev'));
app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  // parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));
//parse requests of content-type - application/json
app.use(bodyParser.json());
// connection to mongodb
mongoose1.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.get('/', function (req, res) {
    res.json({
        status: true,
        message: "Welcome to Recipelicious"
    });
});

app.use('/users',userRouter);
app.use('/recipe',recipeRouter);


app.get('/favicon.ico', function (req, res) {
    res.sendStatus(204);
});

app.use(function (req, res, next) {
    let err = new Error('Not1 Found');
    err.status = 404;
    next(err);
});


// handle errors
app.use(function (err, req, res, next) {
    console.log(err);

    if (err.status === 404)
        res.status(404).json({status: false, message: "Not2 found"});
    else
        res.status(500).json({status: false, message: "Something looks wrong :( !!!"});
});

app.use("error", function(err){ // handle "error" event so nodejs will not crash
    console.log(err);
});

app.listen(8081, function () {
    console.log('Node server listening on port 8081');
});